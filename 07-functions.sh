#!/bin/bash 

## if we assign a name to set of commands then it is called as function.

sample_f() {
    echo Heloo 
    echo Bye
}

## Main Program 
sample_f

## If you declare a variable in main program and in function you can access it 

## If you declare a variabnle in function and in main program you can access, but that function needs to called once.

## If you have a variable declared in main program and the same variable if you declare in function then function can override the main program variable 

## Variables declared as local inside function cannot be accessed from main-program 

## You can declare local variables as 
# local a=20

