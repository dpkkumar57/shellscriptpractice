#!/bin/bash 

## Special variables are $0 to $n , $* , $@, $# , $$,  $?

# $0 -> Inside the script you can access the script name using $0 
echo Name of the script = $0

# $1 -> First argument which is parsed to the script 
echo First Argument = $1
echo Second Argument = $2 
echo Third Argument = $3


# $*, $@ -> All argumnents
echo All Arguments = $*
echo All Arguments = $@ 

# $#  -> Number of arguments
echo Number of Arguments = $#

