#!/bin/bash 

# Printing in shell can be done with either echo command or printf command , But in this demo we will use only echo command because printf is more of syntxes.

echo Hello World from echo 

printf 'Hello World from printf\n'

### Some times we need to print multiple lines from single echo command 
echo Oneline\nNewline

## You are not going to get the output as expected, Because when you have esacpe sequences like \n then you need to enable those with an option is echo is -e 

echo -e Oneline\nNewline

## You still cannot the expected output because -e option expects (not true) input in quotes, Either single or double quotes

echo -e 'Oneline\nNewline'

## If you want two lines you have to give \n\n (\2n is not right in echo command)

# You can also print tab space using \t option.  \t\t for two tabs 
#
echo -e "One\t\t\tTwo"

## Colors printing 
## Text color printing is of two types 
# Color              Background            Foreground
# Red                   31                      41
# Green                 32                      42
# Yellow                33                      43
# Blue                  34                      44
# Magenta               35                      45
# Cyan                  36                      46

## Color enabling in echo can be done with \e escape seq 
# Syntax : echo -e "\e[COLmCONTWENT"

echo -e "\e[31mPrinting in Red Color"
echo -e "\e[33mPrinting in Yellow Color"
