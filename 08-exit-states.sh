#!/bin/bash

## Exit status of every command will be stored in ? variable 
# In order to access ? variable we use $? 

## Exit states ranges from a number 0-255 

# System values are 125+ are system managed values and not preferred by the user to use them.

# 126 -> Permission denined 
# 127 - Command not found.

# 128 + n (kill number signal)
# Ex: Singal No: 143 = 128 + n , n= 143-128 = 15 

# 0 -> Global Success

## We Can also send our own exit status  to command line using exit command
exit 1 