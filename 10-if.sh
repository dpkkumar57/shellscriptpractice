#!/bin/bash 

# If statements uses expressions 
# And expressions are given in [ expression ]

# Expressions are categorized in to three.
# 1. String tests 
#    = , == , !=    [ abc = abc ], [ abc == abc ], [ abc != abc ]
# 2. Number tests
# -eq, -ne, -gt, -ge , -lt, -le [ 1 -eq 0 ]
# 3. File tests
# read the man page or refer https://www.tutorialspoint.com/unix/unix-file-operators.htm


##############

# if command is usually seen in three forms 
## 1. Simple-If
## 2. If-else 
## 3. Else-if 
# Refer for syntaxes https://www.geeksforgeeks.org/conditional-statements-shell-script/

action=$1
file=$2

if [ ! -e "$file" ]; then 
    echo "Given file does not exist, Hence exiting"
    exit 1
fi

if [ "$action" = "start" ]; then 
    echo "Performing start action"
elif [ "$action" = "stop" ]; then 
    echo "Performing stop action"
else 
    echo "Input expected as {start|stop}"
    exit 1
fi 
