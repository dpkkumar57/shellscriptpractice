# shell-scripts

## Follwoing are the shell scripting topics we are going to cover.

1. She-Bang & Comments
2. Printing
3. Variables
4. Inputs
    Special Variables 
    Read
5. Functions
6. Exit States , Quotes, Redirectors 
7. Conditions 
    case 
    if 
8. Loops
9. sed options

Make the script for our web and app server installation.

#### Some more ADHOC options


Task : Install multiple tomcats based on user input 

Means,

In /home/student , if user says 3 then 3 tomcats has to be installed 

/home/student/tomcat1 -> 8080, 8005, 8009 (conf/server.xml)
/home/student/tomcat2 -> 8180, 8105, 8109
/home/student/tomcat3 -> 8280, 8205, 8209

systemctl start tomcat1 


## Disadvantages of shell scripting 
1. Imperative 
2. Platform Issues 
3. Local to execute 
