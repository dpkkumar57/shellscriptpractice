#!/bin/bash 

# Variables are used to store data and be used when ever is needed.

NAME=AWS

## Above statement declared a variable NAME with content in it as DevOps , In order to access that variable we have to use $VARIABLE-NAME in our case it is $NAME or ${NAME}

echo "Welcome to $NAME trainig"
echo "$NAME training timing = 7AM"

# If we assign some data to a name and is called as a variable 

# NAME is a variable in our case and it could be anything 

# Variable names can use the following situations 

#  1. Variables shoould contain characters (alphabets) , numbers and _ (underscore)
#  2. Variable should not start with a number.


# In a case if your variable data is having some special characters then use quotes, either single or double.

NAME="DevOps Training"

echo "Welcome to $NAME"

DATE=2019-07-03

echo "Good Morning, Today is $DATE"

## You can declare variables during run time. It is done with two ways .

# Arithematic Substitution 

ADD=$((2+3))
echo ADD = $ADD 

# Command Substitution 

DATE=$(date +%F)

echo "Today date is $DATE"

echo "Accessing variable declared on Shell, Name = $Name"
## You have to declare variable in shell as export Name=Raju