#!/bin/bash 

# case is a conditionals command in shell scripting 

ACTION=$1

case $ACTION in 
  start) 
    echo -e "Performing \e[31mstart\e[0m of application"
    ;;
  stop) 
    echo -e "Performing \e[31mstop\e[0m of application"
    ;;
  *) 
    echo "Innvalid Input , Provide either start ot stop"
    exit 1
    ;;
esac