#!/bin/bash 

LOG=/tmp/stack.log 
rm -f $LOG 

APP_USER=student
TOMCAT_VERSION=$(curl -s http://tomcat.apache.org/ | grep Released | grep "Tomcat [0-9]" | awk '{print $(NF-1)}' | grep ^8)
TOMCAT_URL="https://archive.apache.org/dist/tomcat/tomcat-8/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz"
TOMCAT_HOME="/home/$APP_USER/apache-tomcat-$TOMCAT_VERSION"

Head() {
    echo -e " \n -----  $1   ------ \n" >>$LOG
    echo -e "\n\t\t\e[1;36m<<\e[4m $1 \e[0m\e[1;36m>>\e[0m"
}

Task_Print() {
    echo -e " \n -----  $1   ------ \n" >>$LOG
    echo -n -e "    $1 =\t"
}

Stat() {
    if [ $1 -eq 0 ]; then 
        echo -e "\e[1;32mSUCCESS\e[0m"
    else 
        echo -e "\e[1;31mFAILURE\e[0m"
        echo "Check the log file :: $LOG fore more information"
        exit 1
    fi
}
## Check whether the script is executed as root user or not 
USER_ID=$(id -u)
if [ $USER_ID -ne 0 ]; then 
    echo "You should be running this script as root user or with sudo command"
    exit 1
fi 

###### WEB SERVER SETUP

Head "WEB SERVER SETUP"
Task_Print "Install Web Server\t\t"
yum install httpd -y &>>$LOG 
Stat $?

Task_Print "Update Proxy Config\t\t"
echo 'ProxyPass "/student" "http://localhost:8080/student"
ProxyPassReverse "/student"  "http://localhost:8080/student"' >/etc/httpd/conf.d/app-proxy.conf 
Stat $?

Task_Print "Update Index File\t\t" 
curl -s https://s3-us-west-2.amazonaws.com/studentapi-cit/index.html -o /var/www/html/index.html 
Stat $?

Task_Print "Start Web Service\t\t" 
systemctl enable httpd &>>$LOG 
systemctl restart httpd &>>$LOG
Stat $?

#### APP SERVER SETUP 

Head "APP SERVER SETUP"
Task_Print "Install Java\t\t"
yum install  java -y &>>$LOG 
Stat $? 

Task_Print "Add Functional User\t\t"
id $APP_USER &>>$LOG 
if [ $? -eq 0 ]; then 
    Stat 0
else 
    useradd $APP_USER &>>$LOG 
    Stat $?
fi 

Task_Print "Download Tomcat\t\t"
cd /home/$APP_USER 
wget -qO- https://archive.apache.org/dist/tomcat/tomcat-8/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz | tar -xz
Stat $? 

Task_Print "Download Student War\t"
cd $TOMCAT_HOME
wget https://s3-us-west-2.amazonaws.com/studentapi-cit/student.war -O webapps/student.war &>>$LOG 
Stat $? 

Task_Print "Download JDBC Connector\t"
cd $TOMCAT_HOME 
wget https://s3-us-west-2.amazonaws.com/studentapi-cit/mysql-connector.jar -O lib/mysql-connector.jar &>>$LOG 
Stat $? 

Task_Print "Update JDBC Connection\t"
cd $TOMCAT_HOME 
sed -i -e '$ i <Resource name="jdbc/TestDB" auth="Container" type="javax.sql.DataSource" maxTotal="100" maxIdle="30" maxWaitMillis="10000" username="USERNAME" password="PASSWORD" driverClassName="com.mysql.jdbc.Driver" url="jdbc:mysql://RDS-DB-ENDPOINT:3306/DATABASE"/>' conf/context.xml &>>$LOG 
Stat $? 

Task_Print "Fix App User Permissions\t"
chown $APP_USER:$APP_USER $TOMCAT_HOME -R 
Stat $? 

Task_Print "Download Tomcat Init Script\t"
wget -q https://s3-us-west-2.amazonaws.com/studentapi-cit/tomcat-init -O /etc/init.d/tomcat &>>$LOG 
chmod +x /etc/init.d/tomcat
Stat $? 

Task_Print "Start Tomcat\t\t"
systemctl daemon-reload
systemctl restart tomcat 
Stat $?
